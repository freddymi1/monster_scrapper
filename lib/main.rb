require 'bundler'
require 'httparty'


class Dofus
	def initialize
		@reponse1 = HTTParty.get('https://fr.dofus.dofapi.fr/monsters?filter[where][type]=Larves')
		@reponse2 = HTTParty.get('https://fr.dofus.dofapi.fr/monsters?filter[where][type]=Monstres des cavernes')
		@reponse3 = HTTParty.get('https://fr.dofus.dofapi.fr/monsters?filter[where][type]=Monstres des Plaines herbeuses')
	end

	#Pour enregistrer les larves dans le fichier larves.json

	def larves

		File.open('./db/Larves.json', 'w') do |x|
			ok = true
			if ok
				x.write(@reponse1)
				puts "Ecriture de larves effectuer avec succes.."
			else
				puts "Probleme de connexion...."
			end
		end
	end

	#Pour enregistrer les monstres des cavernes dans le fichier monster_des_cavernes.json
	def monster_des_cavernes

		File.open('./db/monster_des_cavernes.json', 'w') do |x|
			ok = true
			if ok
				x.write(@reponse2)
				puts "Ecriture de monstres des cavernes effectuer avec succes.."
			else
				puts "Probleme de connexion...."
			end
		end
	end

	#Pour enregistrer les monstres des pleines herbeuses dans le fichier monster_des_pleines_herbeuses.json

	def monster_des_pleines_herbeuses

		File.open('./db/monster_des_pleines_herbeuses.json', 'w') do |x|
			ok = true
			if ok
				x.write(@reponse3)
				puts "Ecriture des fichier monstres des pleines herbeuses effectuer avec succes.."
			else
				puts "Probleme de connexion...."
			end
		end
	end

	#Pour lancer les enregistrement
	def lancer
		larves
		monster_des_cavernes
		monster_des_pleines_herbeuses
	end
end